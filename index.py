# FastAPI will run with Uvicorn
import uvicorn
from fastapi import FastAPI, Request, Form, Body, UploadFile
from pprint import pprint
import json
app = FastAPI()

#def run():
#    uvicorn.run(app, port=8040)

# API process and Jupyter cell compatibility
from multiprocessing import Process
#from wait4it import wait_for

#_api_process = None
#
#def start_api():
#    global _api_process
#    if _api_process:
#        _api_process.terminate()
#        _api_process.join()
#    
#    _api_process = Process(target=run, daemon=True)
#    _api_process.start()
#    wait_for(port=8040)

#def delete_route(method: str, path: str):
#    [app.routes.remove(route) for route in app.routes if method in route.methods and route.path == path]

# Sample endpoint
@app.get("/")
def get_root():
    return {"Hello": "World"}

from pymongo import MongoClient
client = MongoClient()
db    = client.eduardo
users = db.users

from pydantic import BaseModel, Json
from bson import ObjectId
from typing import List, Literal, Set, Union, Dict, Tuple

class ModelUserTinyRequest(BaseModel):
    #_id: ObjectId
    username: str
    location: str
    name: str

class ModelUserInfo(BaseModel):
    username: str
    token: str
    profiles: List[str]

class ModelUserAllRequestModel(ModelUserTinyRequest):
    zeta: ModelUserInfo

import base64
import json
import requests
from requests_http_signature import HTTPSignatureAuth
def get_user_iformation(user: str):
    request = requests.get(
        f"http://localhost:3000/user/{user}",
        auth=HTTPSignatureAuth(
            key    = "aw2Gei9NeePhiel6ohYi1hai",
            key_id = "ooT7loh2ohPh6shopaideeX6"
        )
    )
    assert request.status_code == 200, "Request failed"
    user = request.json()["user_info"]
    return user

"""
FastAPI will use this response_model to:

Convert the output data to its type declaration.
Validate the data.
Add a JSON Schema for the response, in the OpenAPI path operation.
Will be used by the automatic documentation systems
"""
@app.post("/user", response_model=ModelUserTinyRequest)
def post_user(user: ModelUserTinyRequest):
    print("user", user)
    # by_alias: whether field aliases should be used as keys in the returned dictionary; default False
    dict_user_alias = user.dict(by_alias=True)
    users.insert_one(dict_user_alias)
    return user

@app.get("/user/{user}", response_model=ModelUserAllRequestModel)
def get_user(user: str):
    result = users.find_one({"username": user})
    print("result", result)
    data_user_information = get_user_iformation(user)
    result["zeta"] = data_user_information
    print("data_user_information", data_user_information)
    return result

class ModelDelete(BaseModel):
    deleted: bool

@app.delete("/user/{user}", response_model=ModelDelete)
def delete_user(user: str):
    boolean_delete = False
    result_delete = users.delete_one({"username": user})
    print("result_delete",result_delete)
    print(type(result_delete.deleted_count))
    print(result_delete.deleted_count)
    if result_delete.deleted_count==1:
        boolean_delete = True
    result = {"deleted": boolean_delete}
    print(result)
    return result

import uuid
import random

def random_generate(amount: int):
    return [str(uuid.uuid4()) for item in range(amount)]

class ModelFakeUsers(BaseModel):
    count: int

@app.get("/fake/users", response_model=ModelFakeUsers)
def make_fake_user():
    int_count = 0
    location = random_generate(10)
    for row_counter in range(100):
        json_user = {
            "username": random_generate(1)[0],
            "location": random.choice(location),
            "name": random_generate(1)[0]
        }
        json_user_model = ModelUserTinyRequest(**json_user)
        result_post_user = post_user(json_user_model)
        #print("type",type(result_post_user))
        #print("result_post_user", result_post_user)
        int_count += 1
    return {"count": int_count}

"""
aggregate
Aggregation operations process data records and return computed results.
Aggregation operations group values from multiple documents together, and can perform a variety of operations
on the grouped data to return a single result.

count
Passes a document to the next stage that contains a count of the number of documents input to the stage.

$group
Groups input documents by the specified _id expression and for each distinct grouping, outputs a document

$sum
Calculates and returns the sum of numeric values. $sum ignores non-numeric values.
"""
@app.get("/report/users")
def report_users():
    pipeline = {
        "$group" : {
                "_id" : "$location", 
                "count" : { "$sum" : 1}
        }
    }
    result = users.aggregate([pipeline])
    print("result", result)
    return_result = {}
    count: int = 0
    for i in result:
        count += i["count"]
        return_result[i["_id"]] = i["count"]
    return_result["_total"] = count
    return return_result

from typing import Optional

class ModelUserTinyRequestUpdate(BaseModel):
    username: str # fiel id for update
    location: Optional[str]
    name: Optional[str]

# The find() method returns all occurrences in the selection.
@app.put("/user", response_model=bool, response_description="Boolean validate")
def put_user(user: ModelUserTinyRequestUpdate):
    print("ModelUserTinyRequestUpdate", user)
    # values without "None"
    data_update = {k: v for k, v in user.dict().items() if v is not None and k!="username"}
    result_update = users.update_one(
            { "username": user.username }, {"$set": data_update}
    )
    print("data_update",data_update)
    print("result_update", result_update)
    print(users.find())
    if result_update.matched_count==1:
        return True
    return False

if __name__ == '__main__':
    uvicorn.run("index:app", host="0.0.0.0", port=8040, reload=True)