import base64
import requests

#request = requests.get("http://localhost:8040/")
#assert request.status_code == 200, "Invalid HTTP status code"
#assert request.json()["Hello"] == "World", "Expected 'Hello': 'World'"
#print("Test passed!")

# Post testing user
#user = {
#    "name":     "Test User",
#    "username": "test",
#    "location": "Main Office"
#}
#request = requests.post("http://localhost:8000/user", json=user)
#assert request.status_code == 200, "Invalid HTTP status code"
#user = request.json()
#assert user["name"]             == "Test User",   "Invalid name"
#assert user["username"]         == "test",        "Invalid username"
#assert user["location"]         == "Main Office", "Invalid location"
#print("Test passed!")


# curl -X 'GET' \
#  'http://127.0.0.1:8000/user/test' \
#  -H 'accept: application/json'
# Get testing user
#request = requests.get("http://localhost:8000/user/test")
#assert request.status_code == 200, "Invalid HTTP status code"
#user = request.json()
#print(user)
#assert user["name"]             == "Test User",             "Invalid name"
#assert user["username"]         == "test",                  "Invalid username"
#assert user["location"]         == "Main Office",           "Invalid location"
#assert user["zeta"]["username"] == "test",                  "Invalid Zeta username"
#assert user["zeta"]["token"],                               "Invalid Zeta token"
#assert len(user["zeta"]["profiles"]) == 3,                  "Invalid Zeta profiles"
#assert user["zeta"]["profiles"][0]  == "user",              "Invalid Zeta profiles"
#assert user["zeta"]["profiles"][1]  == "orderManagement",   "Invalid Zeta profiles"
#assert user["zeta"]["profiles"][2]  == "mailNotifications", "Invalid Zeta profiles"
#print("Test passed!")


# Verify test user token
#request = requests.post(
#    f"http://localhost:3000/verify/test/{str(base64.b64encode(user['zeta']['token'].encode('utf-8')), 'utf-8')}",
#    auth=HTTPSignatureAuth(
#        key    = "aw2Gei9NeePhiel6ohYi1hai",
#        key_id = "ooT7loh2ohPh6shopaideeX6"
#    )
#)
#assert request.status_code == 200, "Request failed"
#assert request.json()["user_info"]["valid"], "User validation failed"
#print("Test passed!")


# Delete testing user
#request = requests.delete("http://localhost:8000/user/test")
#assert request.status_code == 200, "Invalid HTTP status code"
#response = request.json()
#assert response["deleted"], "Invalid delete response"
#print("Test passed!")

# Create fake users
#request = requests.get("http://localhost:8000/fake/users")
#assert request.status_code == 200, "Invalid HTTP status code"
#response = request.json()
#assert response["count"] == 100, "Invalid count"
#print("Test passed!")

# Report users by location
#request = requests.get("http://localhost:8000/report/users")
#assert request.status_code == 200, "Invalid HTTP status code"
#response = request.json()
#print(response)
#assert len(response.keys()) >= 11,  "Less than 10 locations"
#assert response["_total"]   == 100, "User count is not 100"
#print("Test passed!")

# Post testing user
user = {
    "username":     "0b44c63d-3e83-423d-ac96-93b4ed9f47e3",
    "location": ":)",
    "name": ":)"
}
request = requests.put("http://localhost:8000/user", json=user)
assert request.status_code == 200, "Invalid HTTP status code"
user = request.json()
print(user)
print("Test passed!")